import os
from os.path import join
from multiprocessing import Pool
from labeler import label_ids_2018,merge
import time
import ntpath
from utils import ensure_dir,get_immediate_subdirs, get_num_concurrent_flows


#pcap_dataroot = '/media/juma/data/research/intrusion_detection/dataset/CIC-IDS-2018/PCAPs'
pcap_dataroot = '/home/juma/data/net_intrusion/CIC-IDS-2018/PCAPs'
error_bound =0.0003 
sampler_dir = 'SR_10'

csv_dataroot = pcap_dataroot.replace('PCAPs','CSVs/{}/SGS_e_{}'.format(sampler_dir,error_bound))
executable_dir ='../build/install/SampleMeter/bin' 


def execute(cmd):
    os.system(cmd)

subdirs = get_immediate_subdirs(pcap_dataroot)
#print(subdirs)

cmds = []
for d in subdirs:
    for pcap_filename in os.listdir(join(pcap_dataroot,d)):
        pcap_path = join(pcap_dataroot,d,pcap_filename)
        pcap_dir,_ = ntpath.split(pcap_path)
        output_dir = pcap_dir.replace(pcap_dataroot,csv_dataroot)
        ensure_dir(output_dir)

        nflows = get_num_concurrent_flows(d,pcap_filename)
        LC_size  = nflows//4

        cmd = './SampleMeterCMD "{}" "{}" SGS {} {}'.format(pcap_path,output_dir,error_bound,LC_size)
        cmds.append(cmd)
        print("{:70} - {:20}".format(pcap_path,os.path.getsize(pcap_path)))

os.chdir(executable_dir) # change current working directory

#exit(1)
# multi-processing
tick = time.time()
p = Pool(processes=6)
p.map(execute,cmds)
tock = time.time()
print("Sampling time: {} sec".format(tock - tick))

# merging
merge(csv_dataroot)


#labeling
tick = time.time()
label_ids_2018(csv_dataroot)
tock = time.time()
print("Labeling time: {} sec".format(tock-tick))
