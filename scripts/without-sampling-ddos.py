import os
from os.path import join
from multiprocessing import Pool
from labeler import label_ids_2018, merge
import ntpath
import time
from utils import get_immediate_subdirs, ensure_dir
from utils import get_executables_dir
from utils import get_threshold

dataroot = '/media/juma/data/net_intrusion/ddos19/PCAPs/PCAP-01-12_0750-0818'
#dataroot = '/home/juma/data/net_intrusion/CIC-IDS-2018/PCAPs'

def execute(cmd):
    print(cmd)
    os.system(cmd)


cmds = []
counter=0

output_dir = dataroot.replace('PCAPs','CSVs')
cmd = './cfm "{}" "{}" NS'.format(dataroot,output_dir)


os.chdir(get_executables_dir()) # change current working directory

tick=time.time()
execute(cmd)
tock = time.time()

print("TOTAL Time it took for sampling: {:0.f}  ",(tock-tick)/60.)

