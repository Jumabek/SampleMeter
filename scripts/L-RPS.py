import os
from os.path import join
from multiprocessing import Pool
from labeler import label_ids_2018, merge
import ntpath
import time
from utils import ensure_dir, get_immediate_subdirs,get_num_concurrent_flows
from utils import get_baseline_mem
from utils import get_threshold

#pcap_dataroot = '/media/juma/data/research/intrusion_detection/dataset/CIC-IDS-2018/PCAPs'
pcap_dataroot = '/home/juma/data/net_intrusion/CIC-IDS-2018/PCAPs'
sampling_interval = 10
sampling_rate = int(100./sampling_interval)
sampler_dir = 'SR_{}'.format(sampling_rate)
executable_dir= '../build/install/SampleMeter/bin'

ratio = .0001
csv_dataroot = pcap_dataroot.replace('PCAPs','CSVs_r_{}/{}/RPS_SI_{}'.format(ratio,sampler_dir,sampling_interval))


def execute(cmd):
    print(cmd)
    os.system(cmd)
    

subdirs = get_immediate_subdirs(pcap_dataroot)
print(subdirs)

cmds = []
max_nflows = 0

for d in subdirs:
    for pcap_filename in os.listdir(join(pcap_dataroot,d)):
        pcap_file = join(pcap_dataroot,d,pcap_filename)
        pcap_dir = ntpath.split(pcap_file)[0]
        output_dir = pcap_dir.replace(pcap_dataroot,csv_dataroot)
        ensure_dir(output_dir)
        
        baseline_mem = get_baseline_mem(d,pcap_filename)
        LRU_cache_size = round(baseline_mem*ratio)
        if baseline_mem<get_threshold():
            print("baseline mem is {}".format(get_threshold), d, pcap_filename)
            continue

        cmd = './SampleMeterMemLimitedCMD "{}" "{}" RPS {} {}'.format(pcap_file,output_dir,LRU_cache_size, sampling_interval)
        cmds.append(cmd)

os.chdir(executable_dir)

# multi-processing
#p = Pool(processes=6)
tick = time.time()
#p.map(execute,cmds)
for cmd in cmds:
    execute(cmd)
tock = time.time()
delta = int(tock-tick)
print("TOTAL Time it took for sampling: {} sec ",delta)
open(join(csv_dataroot,'sampling_time.txt'),'w').write(str(delta))
exit()
#merging
tick = time.time()
merge(csv_dataroot)
tock = time.time()
print("Merging time {:.0f} minutes".format((tock-tick)/60.))

#labeling
tick = time.time()
label_ids_2018(csv_dataroot)
tock = time.time()
print("Time take for labeling {} sec".format(tock-tick))
