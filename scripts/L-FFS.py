import os
import glob
from os.path import join
from multiprocessing import Pool
from labeler import label_ids_2018,merge
import time
import ntpath
from utils import ensure_dir, get_immediate_subdirs, get_num_concurrent_flows
from utils import get_baseline_mem
from utils import get_threshold
#pcap_dataroot = '/media/juma/data/research/intrusion_detection/dataset/CIC-IDS-2018/PCAPs'
pcap_dataroot = '/home/juma/data/net_intrusion/CIC-IDS-2018/PCAPs'

s=8
l=16
sampling_interval =4
sampler_dir = 'SR_10'
executable_dir = '../build/install/SampleMeter/bin'

ratio =1

csv_dataroot = pcap_dataroot.replace('PCAPs','CSVs_r_{}/{}/FFS_({},{},{})'.format(ratio, sampler_dir,s,l,sampling_interval))

def execute(cmd):
    os.system(cmd)


subdirs = get_immediate_subdirs(pcap_dataroot)
#print(subdirs)

cmds = []
for d in subdirs:
    for pcap_filename in os.listdir(join(pcap_dataroot,d)):
        pcap_path = join(pcap_dataroot,d,pcap_filename)
        pcap_dir,pcap_file = ntpath.split(pcap_path)
        output_dir = pcap_dir.replace(pcap_dataroot,csv_dataroot)
        ensure_dir(output_dir)

        LC_size  = int(get_num_concurrent_flows(d,pcap_file))//4

        baseline_mem = get_baseline_mem(d,pcap_filename)
        print("{} ".format(baseline_mem),d,pcap_filename)
        LRU_cache_size = round(baseline_mem*ratio)
        if baseline_mem<get_threshold():
            print("#cflows smaller than {}>{} cache for ".format(get_threshold(),baseline_mem),d,pcap_filename)
            continue

        cmd = './SampleMeterMemLimitedCMD "{}" "{}" FFS {} {} {} {} {}'.format(pcap_path,output_dir,LRU_cache_size,sampling_interval, LC_size, s, l)
        cmds.append(cmd)
        #print("{:70} - {:20}".format(pcap_path,os.path.getsize(pcap_path)))

os.chdir(executable_dir)

# multi-processing
tick = time.time()
#p = Pool(processes=6)
#p.map(execute,cmds)
for cmd in cmds:
    execute(cmd)
tock = time.time()
delta = int(tock-tick)
print("Sampling time: {} sec".format(delta))
open(join(csv_dataroot,'sampling_time.txt'),'w').write(str(delta))

# merging
merge(csv_dataroot)


#labeling
tick = time.time()
label_ids_2018(csv_dataroot)
tock = time.time()
print("Labeling time: {} sec".format(tock-tick))
