import os
import glob
from os.path import join
from multiprocessing import Pool
from labeler import label_ids_2018,merge
import time
import ntpath
from utils import ensure_dir, get_immediate_subdirs, get_num_concurrent_flows


pcap_dataroot = '/home/juma/data/net_intrusion/CIC-IDS-2018/PCAPs'
s=10
l=16
sampling_interval =5
sampler_dir = 'SR_10'
csv_dataroot = pcap_dataroot.replace('PCAPs','CSVs/{}/FFS_({},{},{})_IntIP'.format(sampler_dir,s,l,sampling_interval))
executable_dir = '../build/install/SampleMeter/bin'

def execute(cmd):
    os.system(cmd)


subdirs = get_immediate_subdirs(pcap_dataroot)
#print(subdirs)

cmds = []
for d in subdirs:
    for pcap_filename in os.listdir(join(pcap_dataroot,d)):
        pcap_path = join(pcap_dataroot,d,pcap_filename)
        pcap_dir,pcap_file = ntpath.split(pcap_path)
        output_dir = pcap_dir.replace(pcap_dataroot,csv_dataroot)
        ensure_dir(output_dir)

        LC_size  = int(get_num_concurrent_flows(d,pcap_file))//4

        cmd = './SampleMeterCMD "{}" "{}" FFS {} {} {} {}'.format(pcap_path,output_dir,sampling_interval, LC_size, s, l)
        cmds.append(cmd)
        print("{:70} - {:20}".format(pcap_path,os.path.getsize(pcap_path)))

os.chdir(executable_dir)

#exit(1)
# multi-processing
tick = time.time()
p = Pool(processes=6)
p.map(execute,cmds)
tock = time.time()
print("Sampling time: {} sec".format(tock - tick))

# merging
merge(csv_dataroot)


#labeling
tick = time.time()
label_ids_2018(csv_dataroot)
tock = time.time()
print("Labeling time: {} sec".format(tock-tick))
