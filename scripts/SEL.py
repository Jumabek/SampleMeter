import os
from os.path import join
from multiprocessing import Pool
from labeler import label_ids_2018,merge
import time
import ntpath
from utils import ensure_dir, get_immediate_subdirs, get_num_concurrent_flows


#pcap_dataroot = '/media/juma/data/research/intrusion_detection/dataset/CIC-IDS-2018/PCAPs'
pcap_dataroot = '/home/juma/data/net_intrusion/CIC-IDS-2018/PCAPs'
z = 1100
c = 1
n = 1
sampler_dir = 'SR_10'
csv_dataroot = pcap_dataroot.replace('PCAPs','CSVs/{}/SEL_({},{},{})'.format(sampler_dir,z,c,n))
executable_dir ='../build/install/SampleMeter/bin'

def execute(cmd):
    os.system(cmd)


subdirs = get_immediate_subdirs(pcap_dataroot)
#print(subdirs)

cmds = []
for d in subdirs:
    for pcap_filename in os.listdir(join(pcap_dataroot,d)):
        pcap_path = join(pcap_dataroot,d,pcap_filename)
        pcap_dir,pcap_file = ntpath.split(pcap_path)
        output_dir = pcap_dir.replace(pcap_dataroot,csv_dataroot)
        ensure_dir(output_dir)

        LC_size  = int(get_num_concurrent_flows(d,pcap_file))//4
        cmd = './SampleMeterCMD "{}" "{}" SEL {} {} {} {}'.format(pcap_path,output_dir,z,c,n,LC_size)
        print(cmd)
        cmds.append(cmd)

os.chdir(executable_dir) # change current working directory

#exit(1)
# multi-processing
tick = time.time()
p = Pool(processes=6)
p.map(execute,cmds)
#for cmd in cmds:
#    execute(cmd)
tock = time.time()
print("Sampling time: {} sec".format(tock - tick))

# merging
merge(csv_dataroot)


#labeling
tick = time.time()
label_ids_2018(csv_dataroot)
tock = time.time()
print("Labeling time: {} sec".format(tock-tick))
