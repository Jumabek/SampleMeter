import os
from os.path import join

dataroot = '/home/juma/data/net_intrusion/CIC-IDS-2018/'
#dataroot = '/media/juma/data/research/intrusion_detection/dataset/CIC-IDS-2018/'

def get_threshold():
    return 10000

def ensure_dir(path):
    if not os.path.isdir(path):
        try:
            os.makedirs(path)
        except OSError as exc: # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise

def get_immediate_subdirs(a_dir):
    return [name for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]

def get_num_concurrent_flows(d,pcap_file):
    #root = '/home/juma/data/net_intrusion/CIC-IDS-2018/CSVs/without_sampling'
    root = join(dataroot,'CSVs/without_sampling')
    fpath = join(root,d,pcap_file.replace('.pcap','.pcap_WsafSize.txt'))
    with open(fpath,'r') as f:
        n = f.readline()
    return int(n) # it is important to cast to INTEGER (for `MAGNIFY`ing later)

def get_num_concurrent_flows_SR1(d,pcap_file):
    #root = '/home/juma/data/net_intrusion/CIC-IDS-2018/CSVs/without_sampling'
    root = join(dataroot,'/CSVs/SR_1/RPS_SI_100')
    fpath = join(root,d,pcap_file.replace('.pcap','.pcap_WsafSize.txt'))
    with open(fpath,'r') as f:
        n = f.readline()
    return int(n) # it is important to cast to INTEGER (for `MAGNIFY`ing later)

def get_baseline_mem_legacy(d,pcap_file):
    root = join(dataroot,'CSVs/SR_10/RPS_SI_10')
    #root = '/home/juma/data/net_intrusion/CIC-IDS-2018/CSVs/SR_1/RPS_SI_100'
    fpath = join(root,d,pcap_file.replace('.pcap','.pcap_WsafSize.txt'))
    with open(fpath,'r') as f:
        n = f.readline()
    return n # it is important to cast to INTEGER (for `MAGNIFY`ing later)


def get_baseline_mem(d,pcap_file):
    root = join(dataroot,'CSVs/without_sampling')
    #root = '/home/juma/data/net_intrusion/CIC-IDS-2018/CSVs/SR_1/RPS_SI_100'
    fpath = join(root,d,pcap_file.replace('.pcap','.pcap_WsafSize.txt'))
    with open(fpath,'r') as f:
        n = f.readline()
    n = int(n)
    return n # it is important to cast to INTEGER (for `MAGNIFY`ing later)


def get_executables_dir():
    return '../build/install/SampleMeter/bin'

def get_dtype():
    return {'Flow ID':str , 'Dst Port':int , 'Protocol':int,
       'Flow Duration':int , 'Tot Fwd Pkts':int , 'Tot Bwd Pkts': int,
       'TotLen Fwd Pkts':int , 'TotLen Bwd Pkts':int , 'Fwd Pkt Len Max':int,
       'Fwd Pkt Len Min':int , 'Fwd Pkt Len Mean':float , 'Fwd Pkt Len Std':float,
       'Bwd Pkt Len Max':int , 'Bwd Pkt Len Min':int , 'Bwd Pkt Len Mean':float,
       'Bwd Pkt Len Std':float , 'Flow Byts/s':float , 'Flow Pkts/s':float, 'Flow IAT Mean':float,
       'Flow IAT Std':float , 'Flow IAT Max':int , 'Flow IAT Min':int , 'Fwd IAT Tot':int,
       'Fwd IAT Mean':float , 'Fwd IAT Std':float , 'Fwd IAT Max':int , 'Fwd IAT Min':int,
       'Bwd IAT Tot':int , 'Bwd IAT Mean':float , 'Bwd IAT Std':float , 'Bwd IAT Max':int,
       'Bwd IAT Min':int , 'Fwd PSH Flags':int , 'Bwd PSH Flags':int , 'Fwd URG Flags':int,
       'Bwd URG Flags':int , 'Fwd Header Len':int , 'Bwd Header Len':int , 'Fwd Pkts/s':float,
       'Bwd Pkts/s':float , 'Pkt Len Min':int , 'Pkt Len Max':int , 'Pkt Len Mean':float,
       'Pkt Len Std':float , 'Pkt Len Var':float , 'FIN Flag Cnt':int , 'SYN Flag Cnt':int,
       'RST Flag Cnt':int , 'PSH Flag Cnt':int , 'ACK Flag Cnt':int , 'URG Flag Cnt':int,
       'CWE Flag Count':int , 'ECE Flag Cnt':int , 'Down/Up Ratio':float , 'Pkt Size Avg':float,
       'Fwd Seg Size Avg':float , 'Bwd Seg Size Avg':float , 'Fwd Byts/b Avg':float,
       'Fwd Pkts/b Avg':float , 'Fwd Blk Rate Avg':float , 'Bwd Byts/b Avg':float,
       'Bwd Pkts/b Avg':float , 'Bwd Blk Rate Avg':float , 'Subflow Fwd Pkts':float,
       'Subflow Fwd Byts':float , 'Subflow Bwd Pkts':float , 'Subflow Bwd Byts':float,
       'Init Fwd Win Byts':int , 'Init Bwd Win Byts':int , 'Fwd Act Data Pkts':int,
       'Fwd Seg Size Min':int , 'Active Mean':float , 'Active Std':float , 'Active Max':float,
       'Active Min':float , 'Idle Mean':float , 'Idle Std':float , 'Idle Max':float , 'Idle Min':float , 'Label':str}

