import os
from os.path import join
from multiprocessing import Pool
from labeler import label_ids_2018, merge
import ntpath
import time
from utils import ensure_dir, get_immediate_subdirs,get_num_concurrent_flows
from utils import get_baseline_mem
from utils import get_threshold

pcap_dataroot = '/media/juma/data/net_intrusion/ddos19/PCAPs/PCAP-01-12_0750-0818'

sampling_interval = 10
sampling_rate = int(100./sampling_interval)
sampler_dir = 'SR_{}'.format(sampling_rate)
executable_dir= '../build/install/SampleMeter/bin'

kbyte_memory = 1024*1000
mem_for_flow_record = 100*4 # 100 features with float type
LRU_cache_size = kbyte_memory*1000//mem_for_flow_record

csv_dataroot = pcap_dataroot.replace('PCAPs','CSVs_mem_{}kb/{}/RPS_SI_{}'.format(kbyte_memory,sampler_dir,sampling_interval))
ensure_dir(csv_dataroot)

def execute(cmd):
    print(cmd)
    os.system(cmd)
    
cmd = './SampleMeterMemLimitedCMD "{}" "{}" RPS {} {}'.format(pcap_dataroot,csv_dataroot,LRU_cache_size, sampling_interval)

os.chdir(executable_dir)

tick = time.time()
execute(cmd)
tock = time.time()
delta = int(tock-tick)
