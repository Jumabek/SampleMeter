import os
from os.path import join
from multiprocessing import Pool
import ntpath
import math
import time
from labeler import label_ids_2018, merge
from utils import ensure_dir, get_immediate_subdirs, get_num_concurrent_flows
from utils import get_baseline_mem, get_threshold

def round_up(n, decimals=0):
    multiplier = 10 ** decimals
    return math.ceil(n * multiplier) / multiplier

#pcap_dataroot = '/media/juma/data/research/intrusion_detection/dataset/CIC-IDS-2018/PCAPs'
pcap_dataroot = '/home/juma/data/net_intrusion/CIC-IDS-2018/PCAPs'

layer1 = {}
layer1[0]=  1
layer1[1]=  2.14285659790039
layer1[2]=  3.48760619663446
layer1[3]=  5.11604451868295
layer1[4]=  7.15183056968443
layer1[5]=  9.76362232845268  # setting 5/8 % ones trigger saturation will result 9.76 sampling interval
layer1[6]=  13.1804311589001
layer1[7]=  18.0749365561602

layer2 = {num_ones:sampling_interval**2 for num_ones,sampling_interval in layer1.items()}
# {0: 1,
#  1: 4.591834399165235,
#  2: 12.163396982803082,
#  3: 26.17391151714586,
#  4: 51.14868049747272,
#  5: 95.32832097265972,
#  6: 173.72376553450465,
#  7: 326.70333150921635}

layer3 = {num_ones:sampling_interval**3 for num_ones,sampling_interval in layer1.items()}
# {0: 1,
#  1: 9.839642638717196,
#  2: 42.42113868934892,
#  3: 133.90689654978664,
#  4: 365.8066967808472,
#  5: 930.7497231825645,
#  6: 2289.7541322924403,
#  7: 5905.141989715259}

layer4 = {num_ones:sampling_interval**4 for num_ones,sampling_interval in layer1.items()}
# {0: 1,
#  1: 21.08494314935715,
#  2: 147.94822616126314,
#  3: 685.0736441073808,
#  4: 2616.187516632546,
#  5: 9087.488779466437,
#  6: 30179.946711487544,
#  7: 106735.06681922091}


num_of_layers =1
num_of_ones = 5
sampling_interval = layer1[num_of_ones]**num_of_layers
NUM_OF_CORES= 8 
sampler_dir = 'SR_10'
ratio = .001
# CSFID - using Csyyle flow id and hashcode where flowid consists of string concatenation of 5 tuple integers.
# intIP - using obtaining integer based hash where IP adresses are converted to integer then hash code is obtained.

csv_dataroot = pcap_dataroot.replace('PCAPs','CSVs_r_{}/{}/SFS_SI_{}'.format(ratio,sampler_dir,round_up(sampling_interval,2)))
executable_dir = '../build/install/SampleMeter/bin'


def execute(cmd):
    print(cmd)
    os.system(cmd)


subdirs = get_immediate_subdirs(pcap_dataroot)
print(subdirs)

cmds = []
for d in subdirs:
    for pcap_filename in os.listdir(join(pcap_dataroot,d)):
        pcap_path = join(pcap_dataroot,d,pcap_filename)
        pcap_dir,pcap_file = ntpath.split(pcap_path)
        output_dir = pcap_dir.replace(pcap_dataroot,csv_dataroot)
        ensure_dir(output_dir)

        #LC_size = int(get_num_concurrent_flows(d,pcap_file))
        LC_size = round(get_num_concurrent_flows(d,pcap_file)//4)
        #LC_size = get_num_concurrent_flows_SR1(d,pcap_file)//4

        if LC_size<=0:
            #print("ZERO LC_size for",d,pcap_filename)
            continue

        baseline_mem = get_baseline_mem(d,pcap_filename)
        LRU_cache_size = round(baseline_mem*ratio)
        if baseline_mem<get_threshold():
            print("baseline mem of {} is small ".format(baseline_mem),d,pcap_filename)
            continue
        cmd = './SampleMeterMemLimitedCMD "{}" "{}" SFS {} {} {} {} {}'.format(pcap_path,output_dir,LRU_cache_size,sampling_interval, LC_size, num_of_layers, num_of_ones)
        cmds.append(cmd)
        print("{:70} - {:20}".format(pcap_path,os.path.getsize(pcap_path)))

os.chdir(executable_dir) # change current working directory

#exit(1)
tick = time.time()
# multi-processing
#p = Pool(processes=NUM_OF_CORES)
#p.map(execute,cmds)
for cmd in cmds:
    execute(cmd)

tock = time.time()
delta = int(tock - tick)
print("Time it took: {0:.2f}".format(delta))
with open(join(csv_dataroot,'sampling_time.txt'),'w') as f:
    f.write(str(delta))
exit()
# merging
merge(csv_dataroot)

#labeling
tick = time.time()
label_ids_2018(csv_dataroot)
tock = time.time()
print("For labeling it took {} sec".format(tock-tick))
