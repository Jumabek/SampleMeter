import os
from os.path import join
from multiprocessing import Pool
from labeler import label_ids_2018, merge
import ntpath
import time
import math
from utils import ensure_dir, get_immediate_subdirs,get_num_concurrent_flows, get_baseline_mem
from utils import get_executables_dir

def round_up(n, decimals=0):
    multiplier = 10 ** decimals
    return math.ceil(n * multiplier) / multiplier

#pcap_dataroot = '/media/juma/data/research/intrusion_detection/dataset/CIC-IDS-2018/PCAPs'
pcap_dataroot = '/home/juma/data/net_intrusion/CIC-IDS-2018/PCAPs'
sampling_interval = 10
sampling_rate = round_up(100/sampling_interval,1)
sampler_dir = 'SR_{:.0f}'.format(sampling_rate)

csv_dataroot = pcap_dataroot.replace('PCAPs','CSVs/{}/RPS_SI_{}'.format(sampler_dir,sampling_interval))


def execute(cmd):
    print(cmd)
    os.system(cmd)

subdirs = get_immediate_subdirs(pcap_dataroot)
print(subdirs)

cmds = []

for d in subdirs:
    for pcap_filename in os.listdir(join(pcap_dataroot,d)):
        pcap_file = join(pcap_dataroot,d,pcap_filename)
        pcap_dir = ntpath.split(pcap_file)[0]
        output_dir = pcap_dir.replace(pcap_dataroot,csv_dataroot)
        ensure_dir(output_dir)
        cmd = './SampleMeterCMD "{}" "{}" RPS {}'.format(pcap_file,output_dir,sampling_interval)
        cmds.append(cmd)
        print("{:70} - {:20}".format(pcap_file,os.path.getsize(pcap_file)))

os.chdir(get_executables_dir())

# multi-processing
p = Pool(processes=6)
tick = time.time()
p.map(execute,cmds)
tock = time.time()
print("TOTAL Time it took for sampling: {} sec ",tock-tick)

#merging
tick = time.time()
merge(csv_dataroot)
tock = time.time()
print("Merging time {:.0f} minutes".format((tock-tick)/60.))

#labeling
tick = time.time()
label_ids_2018(csv_dataroot)
tock = time.time()
print("Time take for labeling {} sec".format(tock-tick))
