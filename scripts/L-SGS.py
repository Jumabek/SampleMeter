import os
from os.path import join
from multiprocessing import Pool
from labeler import label_ids_2018,merge
import time
import ntpath
from utils import ensure_dir,get_immediate_subdirs, get_num_concurrent_flows
from utils import get_baseline_mem
from utils import get_threshold
#pcap_dataroot = '/media/juma/data/research/intrusion_detection/dataset/CIC-IDS-2018/PCAPs'
pcap_dataroot = '/home/juma/data/net_intrusion/CIC-IDS-2018/PCAPs'
error_bound = .0003
sampler_dir = 'SR_10'
ratio =.001
csv_dataroot = pcap_dataroot.replace('PCAPs','CSVs_r_{}/{}/SGS_e_{}'.format(ratio,sampler_dir,error_bound))
executable_dir ='../build/install/SampleMeter/bin' 


def execute(cmd):
    os.system(cmd)

subdirs = get_immediate_subdirs(pcap_dataroot)
#print(subdirs)

cmds = []
for d in subdirs:
    for pcap_filename in os.listdir(join(pcap_dataroot,d)):
        pcap_path = join(pcap_dataroot,d,pcap_filename)
        pcap_dir,_ = ntpath.split(pcap_path)
        output_dir = pcap_dir.replace(pcap_dataroot,csv_dataroot)
        ensure_dir(output_dir)

        nflows = get_num_concurrent_flows(d,pcap_filename)
        LC_size  = nflows//4

        baseline_mem = get_baseline_mem(d,pcap_filename)
        LRU_cache_size = round(ratio*baseline_mem)
        if baseline_mem<=get_threshold():
            print("smaller than threshold of {}".format(get_threshold()),d,pcap_filename)
            continue
        cmd = './SampleMeterMemLimitedCMD "{}" "{}" SGS {} {} {}'.format(pcap_path,output_dir,LRU_cache_size,error_bound,LC_size)
        cmds.append(cmd)

os.chdir(executable_dir) # change current working directory

# multi-processing
tick = time.time()
#p = Pool(processes=6)
#p.map(execute,cmds)
for cmd in cmds:
    execute(cmd)
tock = time.time()
delta = int(tock - tick)
print("Sampling time: {} sec".format(delta))
with open(join(csv_dataroot,'sampling_time.txt'),'w') as f:
    f.write(str(delta))
exit()
# merging
#merge(csv_dataroot)


#labeling
tick = time.time()
label_ids_2018(csv_dataroot)
tock = time.time()
print("Labeling time: {} sec".format(tock-tick))
