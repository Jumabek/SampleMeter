package cic.cs.unb.ca.jnetpcap;

import cic.cs.unb.ca.jnetpcap.worker.FlowGenListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import static cic.cs.unb.ca.jnetpcap.Utils.LINE_SEP;


public class FlowGeneratorWithLRUCache {
    public static final Logger logger = LoggerFactory.getLogger(FlowGeneratorWithLRUCache.class);

    //total 85 colums
	/*public static final String timeBasedHeader = "Flow ID, Source IP, Source Port, Destination IP, Destination Port, Protocol, "
			+ "Timestamp, Flow Duration, Total Fwd Packets, Total Backward Packets,"
			+ "Total Length of Fwd Packets, Total Length of Bwd Packets, "
			+ "Fwd Packet Length Max, Fwd Packet Length Min, Fwd Packet Length Mean, Fwd Packet Length Std,"
			+ "Bwd Packet Length Max, Bwd Packet Length Min, Bwd Packet Length Mean, Bwd Packet Length Std,"
			+ "Flow Bytes/s, Flow Packets/s, Flow IAT Mean, Flow IAT Std, Flow IAT Max, Flow IAT Min,"
			+ "Fwd IAT Total, Fwd IAT Mean, Fwd IAT Std, Fwd IAT Max, Fwd IAT Min,"
			+ "Bwd IAT Total, Bwd IAT Mean, Bwd IAT Std, Bwd IAT Max, Bwd IAT Min,"
			+ "Fwd PSH Flags, Bwd PSH Flags, Fwd URG Flags, Bwd URG Flags, Fwd Header Length, Bwd Header Length,"
			+ "Fwd Packets/s, Bwd Packets/s, Min Packet Length, Max Packet Length, Packet Length Mean, Packet Length Std, Packet Length Variance,"
			+ "FIN Flag Count, SYN Flag Count, RST Flag Count, PSH Flag Count, ACK Flag Count, URG Flag Count, "
			+ "CWE Flag Count, ECE Flag Count, Down/Up Ratio, Average Packet Size, Avg Fwd Segment Size, Avg Bwd Segment Size, Fwd Header Length,"
			+ "Fwd Avg Bytes/Bulk, Fwd Avg Packets/Bulk, Fwd Avg Bulk Rate, Bwd Avg Bytes/Bulk, Bwd Avg Packets/Bulk,"
			+ "Bwd Avg Bulk Rate,"
			+ "Subflow Fwd Packets, Subflow Fwd Bytes, Subflow Bwd Packets, Subflow Bwd Bytes,"
			+ "Init_Win_bytes_forward, Init_Win_bytes_backward, act_data_pkt_fwd, min_seg_size_forward,"
			+ "Active Mean, Active Std, Active Max, Active Min,"
			+ "Idle Mean, Idle Std, Idle Max, Idle Min, Label";*/

	//40/86
	private FlowGenListener mListener;
	private LRUCache lruCache;

	private boolean bidirectional;
	private long    flowTimeOut;
	private long    flowActivityTimeOut;
	private long    lruCacheSize;
    private long    num_early_kickout_records;

	public FlowGeneratorWithLRUCache(boolean bidirectional, long flowTimeout, long activityTimeout, long cacheSize) {
		super();
		this.bidirectional = bidirectional;
		this.flowTimeOut = flowTimeout;
		this.flowActivityTimeOut = activityTimeout;
		this.lruCacheSize = cacheSize;
        this.num_early_kickout_records = 0;       
		init();
	}		
	
	private void init(){
		lruCache = new LRUCache(this.lruCacheSize);
	}

	public void addFlowListener(FlowGenListener listener) {
		mListener = listener;
	}

    public void addPacket(BasicPacketInfo packet){
        if(packet == null) {
            return;
        }
        
    	BasicFlow   flow;
        BasicFlow returnedFlow=null;
    	long        currentTimestamp = packet.getTimeStamp();

    	if(lruCache.containsKey(packet.getFlowId())){
    		flow = lruCache.getEntry(packet.getFlowId());
    		// Flow finished due flowtimeout: 
    		// 1.- we move the flow to finished flow list
    		// 2.- we eliminate the flow from the current flow list
    		// 3.- we create a new flow with the packet-in-process
    		if((currentTimestamp -flow.getFlowStartTime())>flowTimeOut){
    			if(flow.packetCount()>0){
					if (mListener != null) {
						mListener.onFlowGenerated(flow);
					}/*else{
                        finishedFlows.put(getFlowCount(), flow);
                    }*/
                    //flow.endActiveIdleTime(currentTimestamp,this.flowActivityTimeOut, this.flowTimeOut, false);
    			}
    			lruCache.removeEntry(packet.getFlowId());
    			returnedFlow = lruCache.putEntry(packet.getFlowId(), new BasicFlow(bidirectional,packet,flow.getSrc(),flow.getDst(),flow.getSrcPort(),flow.getDstPort()),currentTimestamp,flowTimeOut);
    			
    			long cfsize = lruCache.size();
    			if(cfsize%50==0) {
    				logger.debug("Timeout current has {} flow",cfsize);
    	    	}
    			
        	// Flow finished due FIN flag (tcp only):
    		// 1.- we add the packet-in-process to the flow (it is the last packet)
        	// 2.- we move the flow to finished flow list
        	// 3.- we eliminate the flow from the current flow list   	
    		}else if(packet.hasFlagFIN()){
    	    	logger.debug("FlagFIN current has {} flow",lruCache.size());
    	    	flow.addPacket(packet);
                if (mListener != null) {
                    mListener.onFlowGenerated(flow);
                } /*else {
                    finishedFlows.put(getFlowCount(), flow);
                }*/
                lruCache.removeEntry(packet.getFlowId());
    		}else{
    			flow.updateActiveIdleTime(currentTimestamp,this.flowActivityTimeOut);
    			flow.addPacket(packet);
                returnedFlow = lruCache.putEntry(packet.getFlowId(), flow,currentTimestamp,flowTimeOut);
    		}
    	}else{
            returnedFlow = lruCache.putEntry(packet.getFlowId(), new BasicFlow(bidirectional,packet),currentTimestamp,flowTimeOut);
    	}

    	//exporting kicked out flow
        if(returnedFlow !=null) {
            if (returnedFlow.packetCount() > 0) {
                if (mListener != null) {
                    logger.debug("Kicked flow with packetCOunt() == ",returnedFlow.packetCount(),"is dumped");
                    mListener.onFlowGenerated(returnedFlow);
                }
                else{
                        System.out.println("mListener is null");
                        System.exit(1);
                    }
                //flow.endActiveIdleTime(currentTimestamp,this.flowActivityTimeOut, this.flowTimeOut, false);
            }
            if(currentTimestamp - returnedFlow.getLastSeen()<flowTimeOut){
                this.num_early_kickout_records+=1;
                if (this.num_early_kickout_records%100==0){
                    System.out.println(String.format("Number of early kickouts so far %d",this.num_early_kickout_records));
                }
            }
        }

    }

    public long  writeNumberOfEarlyKickedRecords(String fileFullPath) {
        if (fileFullPath == null ) {
            String ex = String.format("fullFilePath=%s,filename=%s", fileFullPath);
            throw new IllegalArgumentException(ex);
        }

        File file = new File(fileFullPath);
        FileOutputStream output = null;
        long early_kicked_count = this.num_early_kickout_records;
        try {
            output = new FileOutputStream(file, false);
            output.write(String.valueOf(early_kicked_count).getBytes());
        } catch (IOException e) {
            logger.debug(e.getMessage());
        } finally {
            try {
                if (output != null) {
                    output.flush();
                    output.close();
                }
            } catch (IOException e) {
                logger.debug(e.getMessage());
            }
        }
        return early_kicked_count;
    }

    public int dumpLabeledFlowBasedFeatures(String path, String filename,String header){
    	BasicFlow   flow;
    	int         total = 0;
    	int   zeroPkt = 0;

    	try {
    		//total = finishedFlows.size()+currentFlows.size(); becasue there are 0 packet BasicFlow in the currentFlows

    		FileOutputStream output = new FileOutputStream(new File(path+filename));
			logger.debug("dumpLabeledFlow: ", path + filename);
    		/*output.write((header+"\n").getBytes());
    		Set<Integer> fkeys = finishedFlows.keySet();    		
			for(Integer key:fkeys){
	    		flow = finishedFlows.get(key);
                if (flow.packetCount() > 1) {
                    output.write((flow.dumpFlowBasedFeaturesEx() + "\n").getBytes());
                    total++;
                } else {
                    zeroPkt++;
                }
            }*/
            logger.debug("dumpLabeledFlow finishedFlows -> {},{}",zeroPkt,total);

            Set<String> ckeys = lruCache.getKeySet();
			for(String key:ckeys){
	    		flow = lruCache.getEntry(key);
	    		if(flow.packetCount()>0) {
                    output.write((flow.dumpFlowBasedFeaturesEx() + "\n").getBytes());
                    total++;
                }else{
                    zeroPkt++;
                }

			}
            logger.debug("dumpLabeledFlow total(include current) -> {},{}",zeroPkt,total);
            output.flush();
            output.close();
        } catch (IOException e) {

            logger.debug(e.getMessage());
        }

        return total;
    }       

    public long dumpLabeledCurrentFlow(String fileFullPath,String header) {
        if (fileFullPath == null || header==null) {
            String ex = String.format("fullFilePath=%s,filename=%s", fileFullPath);
            throw new IllegalArgumentException(ex);
        }

        File file = new File(fileFullPath);
        FileOutputStream output = null;
        int total = 0;
        try {
            if (file.exists()) {
                output = new FileOutputStream(file, true);
            }else{
                if (file.createNewFile()) {
                    output = new FileOutputStream(file);
                    output.write((header + LINE_SEP).getBytes());
                }
            }

            Set<String> ckeys = lruCache.getKeySet();
            BasicFlow flow;
            for(String key:ckeys){
                flow = lruCache.getEntry(key);
                if(flow.packetCount()>0) {
                    output.write((flow.dumpFlowBasedFeaturesEx() + LINE_SEP).getBytes());
                    total++;
                }else{

                }
            }

        } catch (IOException e) {
            logger.debug(e.getMessage());
        } finally {
            try {
                if (output != null) {
                    output.flush();
                    output.close();
                }
            } catch (IOException e) {
                logger.debug(e.getMessage());
            }
        }
        return total;
	}

    public long dumpWsafSize(String fileFullPath, long nflows) {
        if (fileFullPath == null ) {
            String ex = String.format("fullFilePath=%s,filename=%s", fileFullPath);
            throw new IllegalArgumentException(ex);
        }

        File file = new File(fileFullPath);
        FileOutputStream output = null;
        int total = 0;
        try {
            output = new FileOutputStream(file, false);
            output.write(String.valueOf(nflows).getBytes());
        } catch (IOException e) {
            logger.debug(e.getMessage());
        } finally {
            try {
                if (output != null) {
                    output.flush();
                    output.close();
                }
            } catch (IOException e) {
                logger.debug(e.getMessage());
            }
        }
        return total;
    }

    public long getWsafSize(){
        return lruCache.size();
    }

    public long dumpSampledPktCount(String fileFullPath, int spc) {
        if (fileFullPath == null ) {
            String ex = String.format("fullFilePath=%s,filename=%s", fileFullPath);
            throw new IllegalArgumentException(ex);
        }

        File file = new File(fileFullPath);
        FileOutputStream output = null;
        int total = 0;
        try {
            output = new FileOutputStream(file, false);
            output.write(String.valueOf(spc).getBytes());
        } catch (IOException e) {
            logger.debug(e.getMessage());
        } finally {
            try {
                if (output != null) {
                    output.flush();
                    output.close();
                }
            } catch (IOException e) {
                logger.debug(e.getMessage());
            }
        }
        return total;
    }
}
