package cic.cs.unb.ca.jnetpcap;
import java.util.Set;

/* package whatever; // don't place package name! */

import afu.org.checkerframework.checker.formatter.FormatUtil;
import afu.org.checkerframework.checker.regex.RegexUtil;

import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class Entry {
    BasicFlow value;
    String key;
    Entry left;
    Entry right;
}
public class LRUCache {
    public static final Logger logger = LoggerFactory.getLogger(LRUCache.class);
    HashMap<String, Entry> hashmap;
    Entry start, end;
    long LRU_SIZE ; // Here i am setting 4 to test the LRU cache
    // implementation, it can make be dynamic
    public LRUCache(long cache_size) {
        LRU_SIZE = cache_size;
        hashmap = new HashMap<String, Entry>();
    }

    public boolean containsKey(String key){
        return hashmap.containsKey(key);
    }

    public long size(){
        return hashmap.size();
    }

    public Set<String> getKeySet(){
        return hashmap.keySet();
    }

    public BasicFlow getEntry(String key) {
        if (hashmap.containsKey(key)) // Key Already Exist, just update the
        {
            Entry entry = hashmap.get(key);
            removeNode(entry);
            addAtTop(entry);
            return entry.value;
        }
        return null;
    }

    public void removeEntry(String key) {
        if (hashmap.containsKey(key)) // Key Already Exist, just update the
        {
            Entry entry = hashmap.get(key);
            removeNode(entry);
            hashmap.remove(key);
        }
        else{
            System.out.println("Attempt to remove non existing entry from cache\n");
            int a = 1/0;
        }
    }

    public BasicFlow putEntry(String key, BasicFlow value, long currentTimestamp, long flowTimeOut) {
        BasicFlow flowToBeKicked=null;
        if (hashmap.containsKey(key)) // Key Already Exist, just update the value and move it to top
        {
            Entry entry = hashmap.get(key);
            entry.value = value;
            removeNode(entry);
            addAtTop(entry);
        } else {
            Entry newnode = new Entry();
            newnode.left = null;
            newnode.right = null;
            newnode.value = value;
            newnode.key = key;
            if (hashmap.size() > LRU_SIZE) // We have reached maxium size so need to make room for new element.
            {
                hashmap.remove(end.key);
                
                flowToBeKicked = end.value;
                removeNode(end);
                addAtTop(newnode);
            } else {
                addAtTop(newnode);
            }

            hashmap.put(key, newnode);
        }
        return flowToBeKicked;
    }

    public void addAtTop(Entry node) {
        node.right = start;
        node.left = null;
        if (start != null)
            start.left = node;
        start = node;
        if (end == null)
            end = start;
    }

    public void removeNode(Entry node) {

        if (node.left != null) {
            node.left.right = node.right;
        } else {
            start = node.right;
        }

        if (node.right != null) {
            node.right.left = node.left;
        } else {
            end = node.left;
        }
    }
//    public static void main(String[] args) throws java.lang.Exception {
//        // your code goes here
//        LRUCache lrucache = new LRUCache();
//        lrucache.putEntry(1, 1);
//        lrucache.putEntry(10, 15);
//        lrucache.putEntry(15, 10);
//        lrucache.putEntry(10, 16);
//        lrucache.putEntry(12, 15);
//        lrucache.putEntry(18, 10);
//        lrucache.putEntry(13, 16);
//
//        System.out.println(lrucache.getEntry(1));
//        System.out.println(lrucache.getEntry(10));
//        System.out.println(lrucache.getEntry(15));
//
//    }

}
