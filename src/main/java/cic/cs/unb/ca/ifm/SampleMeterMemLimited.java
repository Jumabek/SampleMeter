package cic.cs.unb.ca.ifm;

import cic.cs.unb.ca.flow.FlowMgr;
import cic.cs.unb.ca.jnetpcap.*;
import cic.cs.unb.ca.jnetpcap.worker.FlowGenListener;
import cic.cs.unb.ca.jnetpcap.worker.InsertCsvRow;
import isrl.inha.kr.*;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.core.util.Integers;
import org.jnetpcap.PcapClosedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import swing.common.SwingUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static cic.cs.unb.ca.Sys.FILE_SEP;

public class SampleMeterMemLimited {

    public static final Logger logger = LoggerFactory.getLogger(SampleMeterMemLimited.class);
    private static final String DividingLine = "-------------------------------------------------------------------------------";
    private static String[] animationChars = new String[]{"|", "/", "-", "\\"};
    private static Sampler sampler;
    private static int cache_size;
    public static void main(String[] args) {

        long flowTimeout = 120000000L;
        long activityTimeout = 5000000L;
        String rootPath = System.getProperty("user.dir");
        String pcapPath;
        String outPath;
        String sampling_technique;


        /* Select path for reading all .pcap files */
        /*if(args.length<1 || args[0]==null) {
            pcapPath = rootPath+"/data/in/";
        }else {
        }*/

        /* Select path for writing all .csv files */
        /*if(args.length<2 || args[1]==null) {
            outPath = rootPath+"/data/out/";
        }else {
        }*/

        if (args.length < 1) {
            logger.info("Please select pcap!");
            return;
        }
        pcapPath = args[0];
        File in = new File(pcapPath);

        if(in==null || !in.exists()){
            logger.info("The pcap file or folder does not exist! -> {}",pcapPath);
            return;
        }

        if (args.length < 2) {
            logger.info("Please select output folder!");
            return;
        }
        outPath = args[1];
        File out = new File(outPath);
        if (out == null || out.isFile()) {
            logger.info("The out folder does not exist! -> {}",outPath);
            return;
        }

        if (args.length < 3) {
            logger.info("Please select a Sampling Technique: (choices: RPS - Random Packet Sampling, SGS - Sketch Guided Sampling, " +
                    "FFS - Fast Filtered Sampling, SEL - Selective Flow Sampling, SKS - SketchFlow Sampling)");
            return;
        }
        sampling_technique = args[2]; //RPS, FFS, SKS, SGS, SEL
        cache_size = Integers.parseInt(args[3]);
        if (sampling_technique.equals("RPS")) {
            if(args.length<5)
            {
                logger.info("Specify sampling interval for RPS");
                return;
            }
            int sampling_interval = Integers.parseInt(args[4]);
            sampler = new RPS(sampling_interval);
        }
        else if(sampling_technique.equals("FFS")){
            if(args.length<8)
            {
                logger.info("Please specify 1) sampling interval 2) LC size 3) s-small flow size and \n 4) l- large flow size for FFS sampler");
                return;
            }
            int sampling_interval = Integers.parseInt(args[4]);
            int lc_size = Integers.parseInt(args[5]);
            int param_s = Integers.parseInt(args[6]);
            int param_l = Integers.parseInt(args[7]);

            sampler = new FFS(sampling_interval, lc_size, param_s, param_l);
        }
        else if(sampling_technique.equals("SFS")){
            if(args.length<8)
            {
                logger.info("Please specify 4) sampling interval 5) nflows 6) num_layers for SketchFlow Sampler\n" +
                        "7) number of non triggering bits");
                return;
            }
            float sampling_interval = Float.parseFloat(args[4]);
            int lc_size = Integers.parseInt(args[5]);
            int num_layers = Integers.parseInt(args[6]);
            int non_triggering_bits = Integers.parseInt(args[7]);
            sampler = new SFS(lc_size,sampling_interval,num_layers, non_triggering_bits);
        }
        else if (sampling_technique.equals("SGS")) {
            if(args.length<6)
            {
                logger.info("Specify 4) Error Bound and 5) LC_size for SGS");
                return;
            }
            double error_bound = Double.parseDouble(args[4]);
            int LC_size = Integers.parseInt(args[5]);
            sampler = new SGS(error_bound,LC_size);
        }
        else if (sampling_technique.equals("SEL")) {
            if(args.length<8)
            {
                logger.info("Specify 4) z,  5) c, 6) n and 7) LC_size for SEL");
                return;
            }
            float z = Float.parseFloat(args[4]);
            float c = Float.parseFloat(args[5]);
            float n = Float.parseFloat(args[6]);
            int LC_size = Integers.parseInt(args[7]);
            sampler = new SEL(z,c,n,LC_size);
        }

        else{
            System.out.println(String.format("No implementation for sampler %s",sampling_technique));
            return;
        }


        logger.info("You select: {}",pcapPath);
        logger.info("Out folder: {}",outPath);
        logger.info("Sampling Technique: {}",sampling_technique);


        if (in.isDirectory()) {
            readPcapDir(in,outPath,flowTimeout,activityTimeout);
        } else {

            if (!SwingUtils.isPcapFile(in)) {
                logger.info("Please select pcap file!");
            } else {
                logger.info("CICFlowMeter received 1 pcap file");
                readPcapFile(in.getPath(), outPath,flowTimeout,activityTimeout);
            }
        }
    }

    private static void readPcapDir(File inputPath, String outPath, long flowTimeout, long activityTimeout) {
        if(inputPath==null||outPath==null) {
            return;
        }
        File[] pcapFiles = inputPath.listFiles(SwingUtils::isPcapFile);
        int file_cnt = pcapFiles.length;
        System.out.println(String.format("CICFlowMeter found :%d pcap files", file_cnt));

        List<String> pcapFilenames = new ArrayList<String>();
        for(int i=0;i<file_cnt;i++) {
            File file = pcapFiles[i];
            if (file.isDirectory()) {
                continue;
            }
            pcapFilenames.add(file.getPath());
        }
        readPcapFiles(pcapFilenames,outPath, flowTimeout,activityTimeout);

        System.out.println("Completed!");
    }

    private static void readPcapFile(String inputFile, String outPath, long flowTimeout, long activityTimeout) {
        if(inputFile==null ||outPath==null ) {
            return;
        }
        String fileName = FilenameUtils.getName(inputFile);

        if(!outPath.endsWith(FILE_SEP)){
            outPath += FILE_SEP;
        }

        File saveFileFullPath = new File(outPath+fileName+FlowMgr.FLOW_SUFFIX);
        File saveFileWsafSizeFullPath = new File(outPath+fileName+FlowMgr.WSAF_COUNT_SUFFIX);
        File saveFileSPCFullPath = new File(outPath+fileName+FlowMgr.SAMPLED_PKT_COUNT_SUFFIX);

        if (saveFileFullPath.exists()) {
           if (!saveFileFullPath.delete()) {
               System.out.println("Save file can not be deleted");
           }
        }

        FlowGeneratorWithLRUCache flowGen = new FlowGeneratorWithLRUCache(true, flowTimeout, activityTimeout,cache_size);
        flowGen.addFlowListener(new FlowListener(fileName,outPath));
        boolean readIP6 = false;
        boolean readIP4 = true;
        PacketReader packetReader = new PacketReader(inputFile, readIP4, readIP6);

        System.out.println(String.format("Working on... %s",fileName));

        int nValid=0;
        int nTotal=0;
        int sampledTotal=0;
        int nDiscarded = 0;
        long start = System.currentTimeMillis();
        int i=0;
        long maxWsafSize = 0;
        long WsafSize;
        while(true) {
            /*i = (i)%animationChars.length;
            System.out.print("Working on "+ inputFile+" "+ animationChars[i] +"\r");*/
            try{
                BasicPacketInfo basicPacket = packetReader.nextPacket();
                nTotal++;

                    if(basicPacket !=null && (basicPacket.getProtocol()==6 ||basicPacket.getProtocol()==17)){
                        if (sampler.is_sampled(basicPacket)) {
                            sampledTotal++;
                            flowGen.addPacket(basicPacket);
                            // check for num concurrent flows
                            WsafSize = flowGen.getWsafSize();
                            if (WsafSize > maxWsafSize)
                                maxWsafSize = WsafSize;
                        }
                        nValid++;
                    }else{
                        nDiscarded++;
                    }
            }catch(PcapClosedException e){
                break;
            }
            i++;
        }

        System.out.println(String.format("Sampled packet percentage %.2f",100.*sampledTotal/nTotal));

        flowGen.dumpLabeledCurrentFlow(saveFileFullPath.getPath(), FlowFeature.getHeader());
        flowGen.dumpWsafSize(saveFileWsafSizeFullPath.getPath(),maxWsafSize);
        flowGen.dumpSampledPktCount(saveFileSPCFullPath.getPath(),sampledTotal);

        long lines = SwingUtils.countLines(saveFileFullPath.getPath());

        System.out.println(String.format("%s is done. total %d flows ",fileName,lines));
        System.out.println(String.format("Packet stats: Total=%d,Valid=%d,Discarded=%d, Sampled=%d",nTotal,nValid,nDiscarded,sampledTotal));
        System.out.println(DividingLine);

    }

    private static void readPcapFiles(List<String> inputFiles, String outPath, long flowTimeout, long activityTimeout) {
        if(inputFiles.size()<=0 ||outPath==null ) {
            return;
        }
        String filename="records";
        if(!outPath.endsWith(FILE_SEP)){
            outPath += FILE_SEP;
        }

        File saveFileFullPath = new File(outPath+filename+FlowMgr.FLOW_SUFFIX);
        File saveFileWsafSizeFullPath = new File(outPath+filename+FlowMgr.WSAF_COUNT_SUFFIX);
        File saveFileSPCFullPath = new File(outPath+filename+FlowMgr.SAMPLED_PKT_COUNT_SUFFIX);
        File saveFileEarlyKickoutFullPath = new File(outPath+filename+FlowMgr.EARLY_KICKOUT_COUNT); 
        if (saveFileFullPath.exists()) {
            if (!saveFileFullPath.delete()) {
                System.out.println("Save file can not be deleted");
            }
        }

        FlowGeneratorWithLRUCache flowGen = new FlowGeneratorWithLRUCache(true, flowTimeout, activityTimeout,cache_size);
        flowGen.addFlowListener(new FlowListener(filename,outPath));
        boolean readIP6 = false;
        boolean readIP4 = true;

        int nValid = 0;
        int nTotal = 0;
        int sampledTotal = 0;
        int nDiscarded = 0;
        long maxWsafSize = 0;
        long WsafSize;
        int i=0;
        int j;
        int file_cnt = inputFiles.size();
        for(j=0;j<inputFiles.size();j++) {
            String inputFile = inputFiles.get(j);
            int cur = j + 1;
            System.out.println(String.format("==> %d / %d", cur, file_cnt));

            PacketReader packetReader = new PacketReader(inputFile, readIP4, readIP6);

            System.out.println(String.format("Working on... %s", filename));


            while (true) {
            /*i = (i)%animationChars.length;
            System.out.print("Working on "+ inputFile+" "+ animationChars[i] +"\r");*/
                try {
                    BasicPacketInfo basicPacket = packetReader.nextPacket();
                    nTotal++;

                    if (basicPacket != null && (basicPacket.getProtocol() == 6 || basicPacket.getProtocol() == 17)) {
                        if (sampler.is_sampled(basicPacket)) {
                            sampledTotal++;
                            flowGen.addPacket(basicPacket);
                            // check for num concurrent flows
                            WsafSize = flowGen.getWsafSize();
                            if (WsafSize > maxWsafSize)
                                maxWsafSize = WsafSize;
                        }
                        nValid++;
                    } else {
                        nDiscarded++;
                    }
                } catch (PcapClosedException e) {
                    break;
                }
                i++;
            }
        }
        //System.out.println(String.format("Sampled packet percentage %.2f",100.*sampledTotal/nTotal));

        flowGen.dumpLabeledCurrentFlow(saveFileFullPath.getPath(), FlowFeature.getHeader());
        flowGen.dumpWsafSize(saveFileWsafSizeFullPath.getPath(),maxWsafSize);
        flowGen.dumpSampledPktCount(saveFileSPCFullPath.getPath(),sampledTotal);
        flowGen.writeNumberOfEarlyKickedRecords(saveFileEarlyKickoutFullPath.getPath());
        long lines = SwingUtils.countLines(saveFileFullPath.getPath());

        System.out.println(String.format("%s is done. total %d flows ",filename,lines));
        //System.out.println(String.format("Packet stats: Total=%d,Valid=%d,Discarded=%d, Sampled=%d",nTotal,nValid,nDiscarded,sampledTotal));
        System.out.println(DividingLine);

        //long end = System.currentTimeMillis();
        //logger.info(String.format("Done! in %d seconds",((end-start)/1000)));
        //logger.info(String.format("\t Total packets: %d",nTotal));
        //logger.info(String.format("\t Valid packets: %d",nValid));
        //logger.info(String.format("\t Ignored packets:%d %d ", nDiscarded,(nTotal-nValid)));
        //logger.info(String.format("PCAP duration %d seconds",((packetReader.getLastPacket()- packetReader.getFirstPacket())/1000)));
        //int singleTotal = flowGen.dumpLabeledFlowBasedFeatures(outPath, fileName+ FlowMgr.FLOW_SUFFIX, FlowFeature.getHeader());
        //logger.info(String.format("Number of Flows: %d",singleTotal));
        //logger.info("{} is done,Total {} flows",inputFile,singleTotal);
        //System.out.println(String.format("%s is done,Total %d flows", inputFile, singleTotal));
    }

    static class FlowListener implements FlowGenListener {

        private String fileName;

        private String outPath;

        private long cnt;

        public FlowListener(String fileName, String outPath) {
            this.fileName = fileName;
            this.outPath = outPath;
        }

        @Override
        public void onFlowGenerated(BasicFlow flow) {

            String flowDump = flow.dumpFlowBasedFeaturesEx();
            List<String> flowStringList = new ArrayList<>();
            flowStringList.add(flowDump);
            InsertCsvRow.insert(FlowFeature.getHeader(),flowStringList,outPath,fileName+ FlowMgr.FLOW_SUFFIX);

            cnt++;
            String console = String.format("%s -> %d flows \r", fileName,cnt);
            System.out.print(console);
        }
    }


}
